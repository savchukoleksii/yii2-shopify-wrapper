Yii2 Shopify
============
Yii2 Shopify

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist savchukoleksii/yii2-shopify "*"
```

or add

```
"savchukoleksii/yii2-shopify": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \savchukoleksii\AutoloadExample::widget(); ?>```